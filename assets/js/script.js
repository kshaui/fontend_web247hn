$('.product-detail-viewall').click(function() {
    $(this).css('display', 'none');
    $(this).parent().css('position', 'relative');
    $(this).parent().children('.product-detail-viewdefault').css('display', 'inline-block');
    $(this).parent().parent().children('#specifications_detail').css('height','auto');
});
$('.product-detail-viewdefault').click(function() {
    $(this).css('display', 'none');
    $(this).parent().css('position', 'absolute');
    $(this).parent().children('.product-detail-viewall').css('display', 'inline-block');
    $(this).parent().parent().children('#specifications_detail').animate({height: '200px'}, 500);
});
$('.detailProduct-left__slideTop').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.detailProduct-left__slideBottom'
});
$('.detailProduct-left__slideBottom').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.detailProduct-left__slideTop',
    dots: false,
    centerMode: false,
    focusOnSelect: true
});
$('.buyTogether-bottom').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 2000,
  dots: false,
});

$('.categoriesNews-slider').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 2000,
  dots: false,
});

$(".btn-sendcmt").click(function() {

    $(".form").slideToggle();

});


// trả lời cmt form theo từng bình luận ( đoạn js này cần dùng foreach để đổ ra )

$(".btn-sendReply").click(function() {

    $("#form-reply").slideToggle();

});

$(".btn-reply").click(function() {

    $("#form-import").slideToggle();

    $("#form-reply").slideUp();
    
});

// chọn loại hàng trang chi tiết sp ( đoạn js này cần dùng foreach để đổ ra )
$(".species-btn").each(function(){
    $(this).click(function(){
        $(".species-btn").removeClass("active");
        $(this).addClass("active");
    })
});

// js nút active button danh mục trang danh mục tin tức
$(".categoriesNews-btn").each(function(){
    $(this).click(function(){
        $(".categoriesNews-btn").removeClass("active");
        $(this).addClass("active");
    })
});